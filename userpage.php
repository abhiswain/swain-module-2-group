<!DOCTYPE html>
<html>
<head>
    <title>Share Toad Userpage</title>
    <style>
        body{
            font-family: sans-serif;
            font-size: 30px;
            color: white; 
            background-image:url("blackbg.jpg");
            text-align: center;  
        }
    </style>
</head>
<body>
    <?php
        session_start();
        $path=dirname(__FILE__).'/'.$_SESSION['current_user'];
        echo('Welcome back,  '.$_SESSION['current_user']);
        $thelist="";
        if ($handle = opendir($path)) {
            while (($file = readdir($handle))!=false){
                if (($file != ".") && ($file != "..")){
                    $thelist .= '<option value="'.$file.'">'.$file.'</option>';  
                }
            }
            closedir($handle);
        }
    ?>
    <form name="input" action="both.php" method="post">
        <select name="fileToRead">
        <?=(isset($thelist) ? $thelist:null)?>
        </select><br>
        <input type="submit" name="open_button" value="Open">
        <input type="submit" name="delete_button" value="Delete">
    </form>
    <br>
    <a href="sharefile.php" style="color: #7CFC00">Share File.</a>
    <br>
    <br>
    <a href="upload_file.php" style="color: #7CFC00">Upload</a>
    <br>
    <a href="logout.php" style="color: #7CFC00">Log Out</a>
    <br>
    <br>
    <div class="addthis_toolbox addthis_default_style ">
        <a class="addthis_button_tweet"></a>
        <a class="addthis_button_pinterest_pinit"></a>
        <a class="addthis_counter addthis_pill_style"></a>
    </div>
    <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
    <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52373c5b08995f53"></script>
</body>
</html>