<?php

if (isset($_POST['open_button'])){
	session_start();
	$path=dirname(__FILE__).'/'.$_SESSION['current_user'];
	if(isset($_POST['fileToRead']))
		$fileToRead = $_POST['fileToRead'];

	// We need to make sure that the filename is in a valid format; if it's not, display an error and leave the script.
	// To perform the check, we will use a regular expression.
	if( !preg_match('/^[\w_\.\-]+$/', $fileToRead) ){
		echo "Invalid filename";
		exit;
	}

	$full_path = sprintf($path.'/'.$fileToRead);

	// Now we need to get the MIME type (e.g., image/jpeg).  PHP provides a neat little interface to do this called finfo.
	$finfo = new finfo(FILEINFO_MIME_TYPE);
	$mime = $finfo->file($full_path);
	// Finally, set the Content-Type header to the MIME type of the file, and display the file.
	header("Content-Type: ".$mime);
	readfile($full_path);

}


else if (isset($_POST['delete_button'])) {
	 
	session_start();
	$path=dirname(__FILE__).'/'.$_SESSION['current_user'];
	if(isset($_POST['fileToRead']))
		$fileToRead = $_POST['fileToRead'];

	if (file_exists($path.'/'.$fileToRead)) {

		unlink($path.'/'.$fileToRead); // delete it here only if it exists
		echo "The file has been found and deleted";
		echo '<br><a href="userpage.php">Go to Base</a>';
		 

	} else {
		echo "The file was not found and could not be deleted";
		echo '<br><a href="userpage.php">Go to Base</a>';
	}
}


?>
