<?php
session_start();
$path=dirname(__FILE__).'/'.$_SESSION['current_user'];
if(isset($_POST['fileToRead']))
$fileToRead = $_POST['fileToRead']; 

// We need to make sure that the filename is in a valid format; if it's not, display an error and leave the script.
// To perform the check, we will use a regular expression.
if( !preg_match('/^[\w_\.\-]+$/', $fileToRead) ){
	echo "Invalid filename";
	exit;
}
 
 
$full_path = sprintf($path.'/'.$fileToRead);
 
// Now we need to get the MIME type (e.g., image/jpeg).  PHP provides a neat little interface to do this called finfo.
$finfo = new finfo(FILEINFO_MIME_TYPE);
$mime = $finfo->file($full_path);
// Finally, set the Content-Type header to the MIME type of the file, and display the file.
header("Content-Type: ".$mime);
readfile($full_path);
 
?>