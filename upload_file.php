<!DOCTYPE html>
<html>
  <head>
    <title>Share Toad Upload Page</title>
    <style type="text/css">
      body{
        font-family: serif;
        font-size: 30px;
        color: grey; 
        background-image:url("whitebg.jpg");  
      }
    </style>
  </head>
  <body>
    <?php
      session_start();
      $c_user=$_SESSION['current_user'];
      $_FILES["file"]["error"]=0;
      if(isset($_FILES["file"]["name"])){
        if ($_FILES["file"]["error"] > 0){
          echo "Error: " . $_FILES["file"]["error"] . "<br>";
        }
        else{
          echo "Upload: " . (isset($_FILES["file"]["name"]) ? $_FILES["file"]["name"]:"no file selected bro") . "<br>";
          echo "Type: " . (isset($_FILES["file"]["type"]) ? $_FILES["file"]["type"]:"no file selected bro"). "<br>";
          echo "Size: " . (isset($_FILES["file"]["size"]) ? $_FILES["file"]["size"]:"no file selected bro"). "kB<br>";
          echo "\n"; 
        }
        if (file_exists(getcwd().'/'.$c_user.'/'.$_FILES["file"]["name"])){
          echo $_FILES["file"]["name"] . " already exists. ";
        }
        else{
          move_uploaded_file($_FILES["file"]["tmp_name"],
          getcwd().'/'.$c_user.'/'.$_FILES["file"]["name"]);
          echo "Stored in: ".getcwd().'/'.$c_user.'/'.$_FILES["file"]["name"];
          echo "\nFile uploaded successfully !";
        }
      }
    ?>
    <form action="upload_file.php" enctype="multipart/form-data" method="post">
      <label for="file">Filename:</label>
      <input type="file" name="file" id="file"><br>
      <input type="submit" name="submit" value="Submit">
    </form>
    <br>
    <a href="userpage.php">Go to Base</a>

    
  </body>
</html>