<!DOCTYPE html>
<html>
<head>
<title>Share Toad Share Page</title>
<style>
body {
	font-family: serif;
	font-size: 30px;
	color: grey;
	background-image: url("whitebg.jpg");
}
</style>
</head>
<body>

	<?php
	session_start();
	$path=dirname(__FILE__).'/'.$_SESSION['current_user'];
	echo('Sharing is fun,  '.$_SESSION['current_user']. "! ");
	$thelist="";
	if ($handle = opendir($path)){
		while (($file = readdir($handle))!=false){
			if (($file != ".") && ($file != "..")){
				$thelist .= '<option value="'.$file.'">'.$file.'</option>';
			}
		}
		closedir($handle);
	}
	$user1=isset($_POST["user1"])?$_POST["user1"]:null;
	if($user1!=""){
		$file = file_get_contents("usernames.txt");
		$usernameArray = explode(" ", $file);
		if (in_array($user1, $usernameArray)) {
			echo (" Attempting to share "). (isset($_POST['fileToRead'])?$_POST['fileToRead']:null);
			echo (" with user: ") . ($user1)." ... ";
			copy(getcwd().'/'.$_SESSION['current_user'].'/'.$_POST['fileToRead'],getcwd().'/'.$user1.'/'.$_POST['fileToRead']);
			echo "...Sharing Complete!";
		}
		else {
			echo("Incorrect Username, Please Try Again");
		}
	}

	?>
	<form action="sharefile.php" enctype="multipart/form-data"
		method="post">
		<select name="fileToRead">
			<?=(isset($thelist) ? $thelist:null)?>
		</select><br> <input type="text" name="user1"> <input type="submit"
			name="submit" value="Submit">
	</form>
	<br>
	<a href="userpage.php">Go to Base</a>
</body>
</html>